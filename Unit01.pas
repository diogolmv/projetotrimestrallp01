unit Unit01;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    ListBoxSerie: TListBox;
    BttEnv: TButton;
    BttDel: TButton;
    BttAtua: TButton;
    BttSalv: TButton;
    BttCarr: TButton;
    EditNome: TEdit;
    EditTemp: TEdit;
    EditEpi: TEdit;
    ImageFundo: TImage;
    procedure BttEnvClick(Sender: TObject);
    procedure BttAtuaClick(Sender: TObject);
    procedure BttSalvClick(Sender: TObject);
    procedure BttDelClick(Sender: TObject);
    procedure BttCarrClick(Sender: TObject);
    procedure ListBoxSerieClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TSerie = class(TObject)
    nome: string;
    temp, epi: integer;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
procedure TForm1.BttAtuaClick(Sender: TObject);
var serie:TSerie;
var nome, temp, epi: string;
begin
  nome:=EditNome.Text;
  temp:=EditTemp.Text;
  epi:=EditEpi.Text;
  if (nome.Length<>0) and (temp.Length<>0) and (epi.Length<>0) then
  begin
    serie := ListBoxSerie.Items.Objects[ListBoxSerie.ItemIndex] as TSerie;
    serie.nome := nome;
    serie.temp := strtoint(temp);
    serie.epi := strtoint(epi);
    ListBoxSerie.Items[ListBoxSerie.ItemIndex]:=EditNome.Text ;
  end
  else
  begin
    ShowMessage('Ainda existem campos em branco !');
  end;

end;

procedure TForm1.BttCarrClick(Sender: TObject);
var arq:TextFile;
var serie:TSerie;
begin
  AssignFile(arq, 'Series.txt');
  Reset(arq);

  serie:=TSerie.Create;

  while not Eof(arq) do
  begin
      Readln(arq, serie.nome);
      Readln(arq, serie.temp);
      Readln(arq, serie.epi);

      ListBoxSerie.Items.AddObject(serie.nome, serie);
  end;
    CloseFile(arq);
end;

procedure TForm1.BttDelClick(Sender: TObject);
begin
  ListBoxSerie.DeleteSelected;
end;

procedure TForm1.BttEnvClick(Sender: TObject);
var serie2:TSerie;
var nome, temp, epi: string;
begin
  nome:=EditNome.Text;
  temp:=EditTemp.Text;
  epi:=EditEpi.Text;
  if (nome.Length<>0) and (temp.Length<>0) and (epi.Length<>0) then
  begin
    serie2 := TSerie.Create;
    serie2.nome := nome;
    serie2.temp := strtoint(temp);
    serie2.epi := strtoint(epi);
    ListBoxSerie.Items.AddObject(serie2.nome, serie2);
  end
  else
  begin
    ShowMessage('Ainda existem campos em branco !');
  end;

end;

procedure TForm1.BttSalvClick(Sender: TObject);
var arq : TextFile;
var serie:TSerie;
var i: integer;
begin
    if ListBoxSerie.Items.Count<>0 then
    begin
      AssignFile(arq, 'Series.txt');
      Rewrite(arq);
      for i:=0 to Pred(ListBoxSerie.Items.Count) do
      begin
        serie := ListBoxSerie.Items.Objects[i] as TSerie;
        writeln(arq, serie.nome);
        writeln(arq, inttostr(serie.temp));
        writeln(arq, inttostr(serie.epi));
      end;

      CloseFile(arq);
    end
    else
    begin
      ShowMessage('N�o tem nada para salvar !');
    end;

end;
procedure TForm1.ListBoxSerieClick(Sender: TObject);
begin
BttDel.Enabled:=true;
BttAtua.Enabled:=true;

end;

end.
